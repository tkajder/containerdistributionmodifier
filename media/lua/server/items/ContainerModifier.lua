local rollItem = ItemPickerJava.rollItem
local getItemContainer = ItemPickerJava.getItemContainer

local cachedRoomDists = nil

---Overwrite the contents of containers that match the room type, the container type and are within the xyz bounding cube 
--- of a modification in `ContainerModifications` to the loot distribution specified by the modification.
--- The code specifically calls `ItemPickerJava.rollItem` rather than `ItemPickerJava.fillContainer` as to avoid
--- re-firing the container fill event issued by `fillContainer` for compatibility internally and with other mods.
--- Efforts to utilize `ItemPickerJava.ProceduralDistributions` were unsuccessful so `ItemPickerContainer` have to be 
--- retrieved via `Distributions` (SuburbsDistributions) as opposed to `ProceduralDistributions`.
---@param roomType string
---@param containerType string
---@param container ItemContainer
local function overwriteContainerIfLocationMatch(roomType, containerType, container)
        local gridSquare = container:getSourceGrid()

        --- Does not support modifying character/npc inventories
        if gridSquare == nil then
            return
        end

        local x = gridSquare:getX()
        local y = gridSquare:getY()
        local z = gridSquare:getZ()

        for _, modification in ipairs(ContainerModifications) do
            if modification:matchesReplacedRoomName(roomType)
            and modification:matchesReplacedContainerName(containerType)
            and modification:containsLocation(x, y, z) then
                local containerDist = getItemContainer(modification.distributionRoom, modification.distributionContainer, modification.distributionProc, false)
                local junkContainerDist = getItemContainer(modification.distributionRoom, modification.distributionContainer, modification.distributionProc, true)
                local player = getPlayer()

                --- Need to cache updated rooms as we can't `.get` from `THashMap`, but can from `HashMap`
                if cachedRoomDists == nil or not cachedRoomDists:equals(ItemPickerJava.rooms) then
                    cachedRoomDists = HashMap:new(ItemPickerJava.rooms)
                end
                local roomDist = cachedRoomDists:getOrDefault(roomType, nil)

                if containerDist ~= nil then
                    --- Need to sync container contents by going global empty -> local empty -> local fill -> global fill
                    container:removeItemsFromProcessItems()
                    container:emptyIt()
                    
                    if junkContainerDist ~= nil then
                        rollItem(junkContainerDist, container, true, player, roomDist)
                    end

                    rollItem(containerDist, container, true, player, roomDist)
                    container:addItemsToProcessItems()
                end
            end
        end
end

Events.OnFillContainer.Add(overwriteContainerIfLocationMatch)
