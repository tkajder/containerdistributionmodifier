ContainerModifications = {}

ContainerModification = ISBaseObject:derive("ContainerModification")

---Matches the given room name if lowercase matches strings or replacedRoomName is nil.
---@param roomName string
---@return boolean
function ContainerModification:matchesReplacedRoomName(roomName)
    return self.replacedRoomName == nil or string.lower(self.replacedRoomName) == string.lower(roomName)
end

---Matches the given container name if lowercase matches strings or replacedContainerName is nil.
---@param containerName string
---@return boolean
function ContainerModification:matchesReplacedContainerName(containerName)
    return self.replacedContainerName == nil or string.lower(self.replacedContainerName) == string.lower(containerName)
end

--- Whether the given xyz coordinate is contained within the coordinate bounding cube.
---@param x int
---@param y int
---@param z int
---@return boolean
function ContainerModification:containsLocation(x, y, z)
    return self.x1 <= x and x <= self.x2 
        and self.y1 <= y and y <= self.y2
        and self.z1 <= z and z <= self.z2
end

---TOOD
---@param replacedRoomName string
---@param replacedContainerName string
---@param distributionRoom string
---@param distributionContainer string
---@param distributionProc string
---@param x1 int
---@param y1 int
---@param z1 int
---@param x2 int
---@param y2 int
---@param z2 int
---@return table
function ContainerModification:new(replacedRoomName, replacedContainerName, distributionRoom, distributionContainer, distributionProc, x1, y1, z1, x2, y2, z2)
    local o = {}
    setmetatable(o, self)
	self.__index = self

    o.replacedRoomName = replacedRoomName
    o.replacedContainerName = replacedContainerName
    o.distributionRoom = distributionRoom
    o.distributionContainer = distributionContainer
    o.distributionProc = distributionProc
    o.x1 = x1
    o.y1 = y1
    o.z1 = z1
    o.x2 = x2
    o.y2 = y2
    o.z2 = z2

    return o
end

local exampleModification = ContainerModification:new("warehouse", "crate", "chineserestaurant", "restaurantdisplay", "ServingTrayMeatSteamBuns", 10608, 9305, 0, 10626, 9322, 2)
table.insert(ContainerModifications, exampleModification)